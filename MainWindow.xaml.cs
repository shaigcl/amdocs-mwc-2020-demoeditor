﻿//Need to:
//Scale Media from center
//Set Position according to the center of media
//Add Title component


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using System.Xml;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using Microsoft.Win32;

namespace DemoCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string xmlPath;
        private string mediaPath;
        private List<PageItem> pagesData = new List<PageItem>();
       // private List<LayerItem> layersDataPerPage = new List<LayerItem>();
        private List<List<LayerItem>> layersData = new List<List<LayerItem>>();
        private string demoName = "Awsome Demo";
        private string demoFile;

        private double xMin = 0;
        private double xMax = 1300;
        private double yMin = 0;
        private double yMax = 398;


        private double TD_xMin = -1.62962962962963;
        private double TD_xMax = 1.62962962962963;
        private double TD_yMin = 0.5;
        private double TD_yMax = -0.5;

        private bool choosePageflg = false;

        private Label[] medias = new Label[5];

        private List<MediaScale> mediaScales = new List<MediaScale>();
        private List<string> mediaTypes = new List<string>() { "media", "tablet", "mobile","title" , "hdmi" };

        private double scaleUnit;

        private List<string> types = new List<string>() { "media", "tablet", "mobile", "hdmi", "title" };
        private List<string> actions = new List<string>() { "cut", "fade" };
        private List<string> ons = new List<string>() { "0", "1" };
        private List<string> ends = new List<string>() { "stop", "loop" };
        private List<string> effects = new List<string>() { "none", "blur" };
        private List<string> liveTypes = new List<string>() { "ios", "android" };

        private int currentPageID;

        #region ------------------- INIT ----------------------------

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            scaleUnit = MediaCanvas.Height;

            ReadConfig();
            GetFiles();
            SetScales();
            SetMedias();
            ShowGrid(null);
        }

        private void ReadConfig()
        {

            if (File.Exists("config.txt"))
            {

                using (StreamReader sr = new StreamReader("config.txt"))
                {
                    xmlPath = ParseConfig(sr.ReadLine());
                    mediaPath = ParseConfig(sr.ReadLine());
                }
            }
            else
            {
                MessageBox.Show("Missing config file !");
            }
        }

        private string ParseConfig(String str)
        {
            string[] arr = str.Split('='); ;
            return arr[1].Trim();
        }

        private void SetMedias()
        {
            medias[0] = M6;
            medias[1] = M7;
            medias[2] = M8;
            medias[3] = M9;
            medias[4] = M10;
          

            for (int i = 0; i < 5; i++)
            {
              //  medias[i].RenderTransformOrigin = new Point(0.5, 0.5);
                medias[i].Visibility = Visibility.Hidden;
                medias[i].Width = yMax;
                medias[i].Height = yMax;
            }

            double pos = 0;
            imgSkin.SetValue(Canvas.LeftProperty, pos);
            imgSkin.SetValue(Canvas.TopProperty, pos);
            imgSkin.Width = MediaCanvas.Width;
            imgSkin.Height = MediaCanvas.Height;

        }

        private void SetScales()
        {
            MediaScale ms = new MediaScale();
            ms.mediaType = "media";
            ms.mediaName = "media";
            ms.W = 16;
            ms.H = 9;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "hdmi";
            ms.mediaName = "hdmi";
            ms.W = 16;
            ms.H = 9;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "tablet";
            ms.mediaName = "ios";
            ms.W = 4;
            ms.H = 3;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "tablet";
            ms.mediaName = "android";
            ms.W = 16;
            ms.H = 10;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "mobile";
            ms.mediaName = "android";
            ms.W = 10;
            ms.H = 16;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "mobile";
            ms.mediaName = "ios";
            ms.W = 9;
            ms.H = 16;
            mediaScales.Add(ms);

            ms = new MediaScale();
            ms.mediaType = "title";
            ms.mediaName = "title";
            ms.W = 4224;
            ms.H = 1296;
            mediaScales.Add(ms);


        }

        private void GetFiles()
        {
            DirectoryInfo di = new DirectoryInfo(xmlPath);
            FileInfo[] FileList = di.GetFiles("*.xml");

            List<FileNames> fileNames = new List<FileNames>();

            for (int i = 0; i<FileList.Length; i++)
            {
                FileNames fn = new FileNames();
                fn.id = i.ToString();
                fn.name = FileList[i].Name;
                fileNames.Add(fn);
            }
            this.demoDataGrid.ItemsSource = fileNames;

            //demoDataGrid.Items.Add("aaa");
        }

        #endregion

        #region ------------------- DataGrids ----------------------------

        private void DemoDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                FileNames FN = (FileNames)demoDataGrid.CurrentItem;
                demoFile = FN.name;
                ReadXML(demoFile);
                ShowGrid(null);

            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
         
        private void PagesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                PageItem pg = (PageItem)pagesDataGrid.CurrentItem;
                this.layersDataGrid.ItemsSource = null;
               // layersDataGrid.Columns.Clear();
              //  layersDataGrid.Items.Clear();
                layersDataGrid.Items.Refresh();
                currentPageID = Convert.ToInt16(pg.id);
                this.layersDataGrid.ItemsSource = layersData[currentPageID];
                CreateMedia(layersData[currentPageID]);
                ShowGrid(null);
            }
            catch (Exception ex)
            {


            }

        }

        private void LayersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                LayerItem li = (LayerItem)layersDataGrid.CurrentItem;
                //medias
               
            }
            catch (Exception ex)
            {


            }
        }
        
        private void ReadXML(string file)
        {

         //   pagesDataGrid.Columns[0].Header = "Item Type";
            //pagesDataGrid.Columns[0]. = "ItemType";
          //  pagesDataGrid.Columns[0].Width = 100;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath + "/" + file);
            XmlNodeList data = doc.DocumentElement.SelectNodes("//Data");


            XmlNodeList pageNames = doc.DocumentElement.SelectNodes("//Data/Steps/Step"); //Contains all the Screen tags
            pagesData.Clear();
            layersData.Clear();
            for (int i = 0; i < pageNames.Count; i++)
            {
              
                XmlNodeList layers = pageNames[i].ChildNodes;

                PageItem page = new PageItem();
                page.id = i.ToString();
                page.name = pageNames[i].Attributes["Name"].Value;
                demoName = data[0].ChildNodes[0].InnerText;
                txtDemoName.Text = demoName;
                pagesData.Add(page);

                
                // pagesDataGrid.Items.Add(pagesData[0]);
                List<LayerItem> layersDataPerPage = new List<LayerItem>();
               // layersDataPerPage.Clear();
                for (int j = 0; j < layers.Count; j++)
                {
                    LayerItem layer = new LayerItem();
                    layer.pageID = i.ToString();
                    layer.layerID = layers[j]["id"].InnerText;
                    layer.on = layers[j]["on"].InnerText;
                    layer.type = layers[j]["type"].InnerText;
                    layer.action = layers[j]["action"].InnerText;
                    layer.end = layers[j]["end"].InnerText;
                    layer.effect = layers[j]["effect"].InnerText;
                    layer.path = layers[j]["path"].InnerText;
                    layer.initU = layers[j]["initU"].InnerText;
                    layer.initV = layers[j]["initV"].InnerText;
                    layer.initScale = layers[j]["initScale"].InnerText;
                    layersDataPerPage.Add(layer); //Contains all the layer of a specific page
                   // CreateMedia(layer);
                }

                layersData.Add(layersDataPerPage); //2D list that contains all the lyers of all the pages.

            }
            this.pagesDataGrid.ItemsSource = null;
            pagesDataGrid.Columns.Clear();
            pagesDataGrid.Items.Clear();
            pagesDataGrid.Items.Refresh();
            this.pagesDataGrid.ItemsSource = pagesData;
        }
               
        private void CreateMedia(List<LayerItem> layers)
        {

            foreach (LayerItem layer in layers)
            {
                try
                {
                    int layerIndex = Convert.ToInt16(layer.layerID) - 1;
                    bool layerOn = Convert.ToBoolean(Convert.ToInt16(layer.on));
                    if (layerOn)
                    {
                        medias[layerIndex].Visibility = Visibility.Visible;
                        MediaScale ms = GetScale(layer);
                        // medias[layerIndex].Width = ms.W * Convert.ToDouble(layer.initScale);
                        // medias[layerIndex].Height = ms.H * Convert.ToDouble(layer.initScale);

                        ScaleTransform st = new ScaleTransform();
                       // st.CenterX = medias[layerIndex].Width / 2;
                       // st.CenterY = medias[layerIndex].Height / 2;
                        st.ScaleX = Convert.ToDouble(ms.W);
                        st.ScaleY = Convert.ToDouble(ms.H);
                        medias[layerIndex].RenderTransform = st;
                        if (ms.mediaType == "title")
                        {
                            medias[layerIndex].Background.Opacity = 0.5;
                        } else
                        {
                            medias[layerIndex].Background.Opacity = 1;
                        }

                        double xPos = map(Convert.ToDouble(layer.initU)-0.5, TD_xMin, TD_xMax, xMin, xMax);
                        double yPos = map(Convert.ToDouble(layer.initV)+0.5, TD_yMin, TD_yMax, yMin, yMax);
                        medias[layerIndex].SetValue(Canvas.LeftProperty, xPos);
                        medias[layerIndex].SetValue(Canvas.TopProperty, yPos);
                        medias[layerIndex].Content = layer.path;


                        // medias[layerIndex].Margin = new Thickness(xPos, yPos, 0, 0);

                    }
                    else
                    {
                        medias[layerIndex].Visibility = Visibility.Hidden;
                    }
                }
                catch (Exception ex){}
                              
               // medias[layerIndex].
            }
            
        }

        private MediaScale GetScale(LayerItem layer)
        {
            MediaScale ms = new MediaScale();

            ms = mediaScales.Find(x => x.mediaType.Contains(layer.type));

            
            switch (layer.type)
            {
                case "media":
                    // ms = mediaScales.Find(x => x.mediaName.Contains(layer.type));
                    break;
                case "tablet":
                    ms = mediaScales.Find(x => (x.mediaType.Contains(layer.type)) && (x.mediaName.Contains(layer.path)));
                     break;
                case "mobile":
                    ms = mediaScales.Find(x => (x.mediaType.Contains(layer.type)) && (x.mediaName.Contains(layer.path)));
                    break;
                case "title":
                  //  ms = mediaScales.Find(x => x.mediaName.Contains(layer.type));
                    break;
            }

            try
            {
                double ww = ms.W;
                double hh = ms.H;

               // ms.H = scaleUnit * Convert.ToDouble(layer.initScale);
                ms.H = Convert.ToDouble(layer.initScale);
                ms.W = (ms.H * ww) / hh;
            }
            catch (Exception ex){}
                      
            return ms;
        }

        private double map(double value, double low1, double high1, double low2, double high2)
        {
            if (value == low1)
            if (value == low1)
            {
                return low2;
            }

            double range1 = high1 - low1;
            double range2 = high2 - low2;
            double result = value - low1;
            double ratio = result / range1;
            result = ratio * range2;
            result = result + low2;
            return result;
        }
        
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            
            int s, i;

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter xmlWriter = XmlWriter.Create(xmlPath + "/" + demoFile, settings);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Data");
           
            xmlWriter.WriteElementString("DemoName", txtDemoName.Text);
      

            xmlWriter.WriteStartElement("Steps");

            for (s = 0; s < pagesData.Count; s++)
            {
              
          //  xmlWriter.WriteElementString("screenID", s.ToString());
          //  xmlWriter.WriteElementString("screenType", playLists[s][0].PlayerType);
            xmlWriter.WriteStartElement("Step");
                xmlWriter.WriteAttributeString("ID", pagesData[s].id);
                xmlWriter.WriteAttributeString("Name", pagesData[s].name);
                for (i = 0; i < layersData[s].Count; i++)
                {

                    xmlWriter.WriteStartElement("Layer");
                   
                    xmlWriter.WriteElementString("id", layersData[s][i].layerID);
                    xmlWriter.WriteElementString("on", layersData[s][i].on);
                    xmlWriter.WriteElementString("type", layersData[s][i].type );
                    xmlWriter.WriteElementString("action", layersData[s][i].action);
                    xmlWriter.WriteElementString("end", layersData[s][i].end);
                    xmlWriter.WriteElementString("effect", layersData[s][i].effect);
                    xmlWriter.WriteElementString("path", layersData[s][i].path);
                    xmlWriter.WriteElementString("initU", layersData[s][i].initU);
                    xmlWriter.WriteElementString("initV", layersData[s][i].initV);
                    xmlWriter.WriteElementString("initScale", layersData[s][i].initScale);
                    xmlWriter.WriteEndElement();//Layer
                }
                xmlWriter.WriteEndElement();  //Step

            }
            xmlWriter.WriteEndElement();    //Steps

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            CreateMedia(layersData[currentPageID]);
        }


        #endregion

        private DataGridCell cell = new DataGridCell();
        private int currentRowIndex;
        private int currentColIndex;
        private string currentValue;

        private void LayersDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
            DependencyObject dep = (DependencyObject)e.OriginalSource;

            // iteratively traverse the visual tree
            while ((dep != null) &
            !(dep is DataGridCell) & 
            !(dep is DataGridColumnHeader))
    {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
                return;

            if (dep is DataGridColumnHeader)
            {
                DataGridColumnHeader columnHeader = dep as DataGridColumnHeader;
                // do something
            }

            if (dep is DataGridCell)
            {
                cell = dep as DataGridCell;

                // navigate further up the tree
                while ((dep != null) & !(dep is DataGridRow))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }

                DataGridRow row = dep as DataGridRow;

                currentRowIndex = FindRowIndex(row);
                currentColIndex = cell.Column.DisplayIndex;
                currentValue = ExtractBoundValue(row, cell);

                switch (currentColIndex)
                {
                    case 0:
                        ShowGrid(null);
                        lblChoose.Content = "";
                        break;
                    case 1:
                        ShowGrid(null);
                        lblChoose.Content = "";
                        break;
                    case 2: //on
                        ShowGrid(grdComboType);
                        ShowComboOptions(ons);
                        lblChoose.Content = "Choose if layer in on";
                        break;
                    case 3: //type
                        ShowGrid(grdComboType);
                        ShowComboOptions(types);
                        lblChoose.Content = "Choose type";
                        break;
                    case 4: //action
                        ShowGrid(grdComboType);
                        ShowComboOptions(actions);
                        lblChoose.Content = "Choose action";


                        break;
                    case 5: //end
                        ShowGrid(grdComboType);
                        ShowComboOptions(ends);
                        lblChoose.Content = "Choose end behaviour";
                        break;
                    case 6: //effect
                        ShowGrid(grdComboType);
                        ShowComboOptions(effects);
                        lblChoose.Content = "Choose entrance effect";
                        break;
                    case 7: //path
                        if ((layersData[currentPageID][currentRowIndex].type == "tablet") || (layersData[currentPageID][currentRowIndex].type == "mobile"))
                        {
                            ShowGrid(grdComboType);
                            ShowComboOptions(liveTypes);
                            lblChoose.Content = "Choose device type";
                        } else
                        {
                            ShowGrid(grdBrowse);
                            txtFile.Text = currentValue;
                            lblChoose.Content = "Choose media file";
                        }

                        break;
                    case 8: //initU - X
                        ShowGrid(grdNumeric);
                       // txtValue.Text = currentValue;
                        btnUp.Visibility = Visibility.Hidden;
                        btnDown.Visibility = Visibility.Hidden;
                        btnLeft.Visibility = Visibility.Visible;
                        btnRight.Visibility = Visibility.Visible;
                        lblChoose.Content = "Set X position";
                        break;
                    case 9: //initV - Y
                        ShowGrid(grdNumeric);
                      //  txtValue.Text = currentValue;
                        btnUp.Visibility = Visibility.Visible;
                        btnDown.Visibility = Visibility.Visible;
                        btnLeft.Visibility = Visibility.Hidden;
                        btnRight.Visibility = Visibility.Hidden;
                        lblChoose.Content = "Set Y position";
                        break;
                    case 10: //initScale
                        ShowGrid(grdNumeric);
                      //  txtValue.Text = currentValue;
                        btnUp.Visibility = Visibility.Visible;
                        btnDown.Visibility = Visibility.Visible;
                        btnLeft.Visibility = Visibility.Hidden;
                        btnRight.Visibility = Visibility.Hidden;
                        lblChoose.Content = "Set scale";
                        break;

                }

                Console.WriteLine(currentRowIndex + " ; " + currentColIndex + " ; " + currentValue);
            }


           



        }

        private void ShowGrid(Grid grd)
        {
            grdComboType.Visibility = Visibility.Hidden;
            grdBrowse.Visibility = Visibility.Hidden;
            grdNumeric.Visibility = Visibility.Hidden;

            if (grd !=null)
            {
                grd.Visibility = Visibility.Visible;
            } else
            {
                lblChoose.Content = "";
            }
           

        }
        
        private int FindRowIndex(DataGridRow row)
        {
            DataGrid dataGrid =
                ItemsControl.ItemsControlFromItemContainer(row)
                as DataGrid;

            int index = dataGrid.ItemContainerGenerator.
                IndexFromContainer(row);

            return index;
        }

        private string ExtractBoundValue(DataGridRow row,DataGridCell cell)
        {
            // find the column that this cell belongs to
            DataGridBoundColumn col =
               cell.Column as DataGridBoundColumn;

            // find the property that this column is bound to
            Binding binding = col.Binding as Binding;
            string boundPropertyName = binding.Path.Path;

            // find the object that is related to this row
            object data = row.Item;

            // extract the property value
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(data);

            PropertyDescriptor property = properties[boundPropertyName];
            string value = property.GetValue(data).ToString();

            return value;
        }

        #region ------------------- Combo Type Grid ----------------------------
        
        private void ShowComboOptions(List<string> lst)
        {
            cmbOptions.Items.Clear();

            for (int i = 0; i<lst.Count; i++)
            {
                cmbOptions.Items.Add(lst[i]);
            }

            cmbOptions.Text = currentValue;


        }

        private void CmbOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            string text = (sender as ComboBox).SelectedItem as string;
            cell.Content = text;

            switch (currentColIndex)
            {
                case 2: //on
                    layersData[currentPageID][currentRowIndex].on = text;
                    break;
                case 3: //type
                    layersData[currentPageID][currentRowIndex].type = text;
                    break;
                case 4: //action
                    layersData[currentPageID][currentRowIndex].action = text;
                    break;
                case 5: //end
                    layersData[currentPageID][currentRowIndex].end = text;
                    break;
                case 6: //effect
                    layersData[currentPageID][currentRowIndex].effect = text;
                    break;

                case 7: //path
                    layersData[currentPageID][currentRowIndex].path = text;
                    break;

                case 8: //initU
                    break;
                case 9: //initV
                    break;
                case 10: //initScale
                    break;

            }
            CreateMedia(layersData[currentPageID]);
        }

        #endregion

        #region ------------------- Browse Grid ----------------------------

        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = mediaPath;

            if (openFileDialog.ShowDialog() == true)
            {
                txtFile.Text = openFileDialog.SafeFileName;
                layersData[currentPageID][currentRowIndex].path = txtFile.Text;
                cell.Content = txtFile.Text;
            }
                 
        }

        #endregion

        #region ------------------- Numeric Grid ----------------------------

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {
           SetPos(1);
        }


        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            SetPos(-1);
        }

        private void BtnRight_Click(object sender, RoutedEventArgs e)
        {
            SetPos(1);
        }

        private void BtnLeft_Click(object sender, RoutedEventArgs e)
        {
            SetPos(-1);
        }

        private void SetPos(int dir)
        {
            double step = Convert.ToDouble(txtStep.Text) / 1296;
            double newValue = Convert.ToDouble(currentValue) + (step*dir);
            cell.Content = newValue;
            currentValue = newValue.ToString();
            SetNewValue(newValue);
            CreateMedia(layersData[currentPageID]);

        }

     
        private void SetNewValue(double newVal)
        {
            switch (currentColIndex)
            {
               
                case 8: //initU
                    layersData[currentPageID][currentRowIndex].initU = newVal.ToString();
                    break;
                case 9: //initV
                    layersData[currentPageID][currentRowIndex].initV = newVal.ToString();
                    break;
                case 10: //initScale
                    layersData[currentPageID][currentRowIndex].initScale = newVal.ToString();
                    break;

            }
           
        }

        #endregion


    }



    public class FileNames
    {
        public string id { get; set; }
        public string name { get; set; }

    }

    public class PageItem
    {
        public string id { get; set; }
        public string name { get; set; }
      
    }

    public class LayerItem
    {
        public string pageID { get; set; }
        public string layerID { get; set; }
        public string on { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string end { get; set; }
        public string effect { get; set; }
        public string path { get; set; }
        public string initU { get; set; }
        public string initV { get; set; }
        public string initScale { get; set; }


    }

    public class MediaScale
    {
        public string mediaType { get; set; }
        public string mediaName { get; set; }
        public double W { get; set; }
        public double H { get; set; }
    }

}
